#include <iostream>
#include <set>
#include <string>
#include <iterator>

#include "subsets.hpp"

int main(int argc, char **argv)
{
  std::set< std::string > args;
  for (int i = 1; i < argc; i++)
    {
      args.insert(std::string(argv[i]));
    }

  for (auto s : cs474_574::subsets(args))
    {
      std::cout << "[";
      std::copy(s.begin(), s.end(), std::ostream_iterator< std::string >(std::cout, " "));
      std::cout << "]" << std::endl;
    }
}
