#ifndef __CS474574_SUBSETS_CPP_
#define __CS474574_SUBSETS_CPP_

#include <algorithm>

namespace cs474_574
{
  template< typename T >
  void subsets_helper(std::vector< size_t >& indices, std::vector< std::vector< T > >& result, std::vector< T >& elts, size_t curr_count, size_t target_count);

  template <typename T >
  typename std::vector< std::vector< typename T::value_type > > subsets(const T& container)
  {
    // copy into a vector so we can index
    std::vector< typename T::value_type > elts;
    std::copy(container.begin(), container.end(), std::back_inserter(elts));

    std::vector< std::vector< typename T::value_type > > result;
    for (size_t i = 0; i <= elts.size(); i++)
      {
	// make a vector of indices into elts to select from, initially 0,1,...
	std::vector< size_t > indices;
	for (size_t i = 0; i < elts.size(); i++)
	  {
	    indices.push_back(i);
	  }

	// recursive function adds all possible subsets of size i
	subsets_helper(indices, result, elts, 0, i);
      }

    return result;
  }

  template< typename T >
  void subsets_helper(std::vector< size_t >& indices, std::vector< std::vector< T > >& result, std::vector< T >& elts, size_t curr_count, size_t target_count)
  {
    if (curr_count == target_count)
      {
	// prefix of indices are what we want
	std::vector< T > subset;
	for (size_t i = 0; i < curr_count; i++)
	  {
	    subset.push_back(elts[indices[i]]);
	  }
	result.push_back(subset);
      }
    else
      {
	// iterate over all possible next elements
	for (size_t i = curr_count; i < indices.size(); i++)
	  {
	    // enforce selecting elements from elts in order of increasing
	    // index so we get each subset only once in the output
	    if (curr_count == 0 || indices[i] > indices[curr_count - 1])
	      {
		std::swap(indices[curr_count], indices[i]);
		subsets_helper(indices, result, elts, curr_count + 1, target_count);
		std::swap(indices[curr_count], indices[i]);
	      }
	  }
      }
  }
}

#endif

