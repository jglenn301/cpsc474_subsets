// I find Java's package system cumbersome, so I'm leaving this in the
// default package.  You can move it to wherever you wish to move it to.

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class CombinatoricsTools
{
    /**
     * Returns a collection of collections representing all possible subsets
     * of the elements returned when iterating over the given iterable.
     *
     * @param source a finite collection of elements
     * @return a list of lists of elements from source
     */
    public static <T> List< List< T > > subsets(Iterable< T > source)
    {
	List< List< T > > result = new ArrayList< List < T > >();

	// dump elements into a list
	List< T > elts = new ArrayList< T >();
	source.forEach(elts::add);
	
	// make array of indices for helper to permute
	int[] indices = new int[elts.size()];
	for (int i = 0; i < indices.length; i++)
	    {
		indices[i] = i;
	    }

	// helper function adds all subsets of elts of each possible size
	for (int sz = 0; sz <= elts.size(); sz++)
	    {
		subsetsHelper(indices, result, elts, 0, sz);
	    }

	return result;
    }

    /**
     * Adds all subsets of the given elements of the given target size
     * that contain the elements from elts at the indices given in the first
     * currCount locations of indices and additional elements with
     * indices strictly greater than those.
     *
     * @param indices a permutation of 0, ..., size of elts - 1
     * @param result a set of iterables
     * @param elts a list of items
     * @param currCount an integer between 0 and the size of elts inclusive
     * @param targetCount an integer between currCount and the size of elts
     * inclusive
     */
    private static <T> void subsetsHelper(int[] indices,
					  List< List< T > > result,
					  List< T > elts,
					  int currCount,
					  int targetCount)
    {
	if (currCount == targetCount)
	    {
		// have the right number of elements; copy them into a set
		// and add that set to the result
		List< T > subset = new ArrayList< T >();
		for (int i = 0; i < currCount; i++)
		    {
			subset.add(elts.get(indices[i]));
		    }
		result.add(subset);
	    }
	else
	    {
		// iterate over all possible next elements
		for (int i = currCount; i < indices.length; i++)
		    {
			// enforce selecting elements from elts in
			// order of increasing index so we get each
			// subset only once in the output
			if (currCount == 0 || indices[i] > indices[currCount - 1])
			    {
				swap(indices, currCount, i);
				subsetsHelper(indices, result, elts, currCount + 1, targetCount);
				swap(indices, currCount, i);
			    }
		    }
	    }
    }

    /**
     * Swaps two elements in an array.
     *
     * @param arr an array of ints
     * @param i an index in that array
     * @param j an index in that array
     */
    private static void swap(int[] arr, int i, int j)
    {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
    }

    public static void main(String[] args)
    {
	List< String > elts = Arrays.asList(args);
	for (List< String > subset : subsets(elts))
	    {
		System.out.println(subset);
	    }
    }
}
