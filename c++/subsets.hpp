#ifndef __CS474574_SUBSETS_HPP__
#define __CS474574_SUBSETS_HPP__

#include <vector>

namespace cs474_574
{
  template <typename T>
  typename std::vector< std::vector< typename T::value_type > > subsets(const T& container);
}

#include "subsets.cpp"

#endif
